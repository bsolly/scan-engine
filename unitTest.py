from cryptography.fernet import Fernet

def utEncrypt(key,pw):
    k = Fernet(key)
    token = k.encrypt(pw.encode())
    return token

def utDecrypt(key,token):
    dk = Fernet(key)
    c = dk.decrypt(token)
    return c

def main():
    b = input("Enter your password: ")
    k = Fernet.generate_key()
    tk = utEncrypt(k,b)

    print("Token is ", tk)
    print("Decrypted pass: ",utDecrypt(k,tk))

if __name__ == '__main__':
    main()


